/// <reference types="cypress" />

const faker = require('faker');

/* global Then, When, Given */

let user = { email: faker.internet.email(),
  name: {
    first: faker.name.firstName(),
    last: faker.name.lastName()
  }}

When('informe meu email para cadastro de usuario', () => {
  cy.get('#email_create').type(`${user.email}{enter}`)
})

And('finalizo o cadastro de usuario preenchendo todos os dados', () => {
  cy.url().should('include', '#account-creation') // => true
  cy.get('#email').should('include.value', user.email)
    .get('#id_gender1').check()
    .get('#customer_firstname').type(user.name.first)
    .get('#customer_lastname').type(user.name.last)
    .get('#passwd').type(faker.internet.password())
    .get('#address1').type(faker.address.streetAddress())
    .get('#city').type(faker.address.cityName())
    .get('#id_state').select('Florida').should('have.value', '9')
    .get('#postcode').type(`${faker.datatype.number({min:10000, max:99999})}`)
    .get('#phone_mobile').type(faker.phone.phoneNumberFormat())
    .get('#submitAccount > span').click()
})

Then('O sistema realiza meu cadastro com sucesso me autenticando na pagina', () => {
  cy.get('.account > span')
    .should('have.text', `${user.name.first} ${user.name.last}`)
})





