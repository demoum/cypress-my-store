Feature: Cadastro de Usuario

  Eu como consumidor desejo realizar cadastro no site para realizar compras

  Scenario: Cadastrar usuario validado
    Given cliquei para me inscrever na loja
    When informe meu email para cadastro de usuario
    And finalizo o cadastro de usuario preenchendo todos os dados
    Then O sistema realiza meu cadastro com sucesso me autenticando na pagina

  Scenario: Cadastrar usuario com email invalido
    Given cliquei para me inscrever na loja
    When tentei me inscrever com um email invalido
    Then o sistema notifica  que o email utilizado é invalido

  Scenario: Cadastrar usuario com email existente
    Given cliquei para me inscrever na loja
    When tentei me inscrever com um email que ja esta em uso
    Then o sistema notifica  que o email ja esta sendo utilizado por outro usuario