/// <reference types="cypress" />

const faker = require('faker');

describe('Cadastro com Sucesso', () => {
  const usuario = { email: faker.internet.email()}

  before(() => {
    cy.visit('http://automationpractice.com/index.php')
  })
  it("Informar novo e-mail", () => {
      cy.get('.login').click()
        .get('#email_create').type(`${usuario.email}{enter}`)
  })

  it('Preencher dados pessoais', () => {
    cy.url().should('include', '#account-creation') // => true
    cy.get('#email').should('include.value', usuario.email)
      .get('#id_gender1').check()
      .get('#customer_firstname').type(faker.name.firstName())
      .get('#customer_lastname').type(faker.name.lastName())
      .get('#passwd').type(faker.internet.password())
  })

  it('Preencher Endereço e Contato', () => {
    cy.get('#address1').type(faker.address.streetAddress())
      .get('#city').type(faker.address.cityName())
      .get('#id_state').select('Florida').should('have.value', '9')
      .get('#postcode').type(`${faker.datatype.number({min:10000, max:99999})}`)
      .get('#phone_mobile').type(faker.phone.phoneNumberFormat())
    })
  
    it('Finalizar Cadastro', () => {
      cy.get('#submitAccount > span').click();
    })
})
