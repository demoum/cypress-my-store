/// <reference types="cypress" />

it ("cy.contains() - Selecionando Elementos", () => {
  cy.visit('?id_category=5&controller=category')
      .get('#ul_layered_id_attribute_group_3')
        .find('li')
          .contains('Orange')
            .find('span')
})