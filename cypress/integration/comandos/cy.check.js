/// <reference types="cypress" />

it ("Escolhendo uma opção do Checkbox", () => {
  cy.visit('?id_category=5&controller=category')
    .get('#ul_layered_id_attribute_group_1')
      .find("[type=checkbox]")
        .each((checkbox) => {
          cy.get(checkbox)
            .check()
        })
})

it ("Escolhendo uma opção do Radio", () => {
  cy.visit('?controller=authentication&back=my-account')
    .get('#email_create')
      .type("email_qualquer@qazando.com.br{enter}")
        .get('#id_gender1')
        .check()
})

