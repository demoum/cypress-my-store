/// <reference types="cypress" />

it ("Escolhendo uma opção do Select", () => {
  cy.visit('?id_category=5&controller=category')
    .get('#selectProductSort')
      .select('In stock')
})

it("Escolhendo multiplas opções do Select", () => {
  cy.visit(`http://slimselectjs.com/?p=%2Fmethods%`)
    .get('#slim-multi-select')
    .select(['Best','Ever'],{force: true})
})